package com.baomidou.mybatisplus.samples.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * mysql 代码生成器演示例子
 * </p>
 *
 * @author gblfy
 * @since 2019-07-12
 */
public class OracleGenerator {


    /*************************************ORACLE代码生成  Start *************************************/

    // 全局配置
    private final static String OUTPUT_XML_DIR = "/src/main/resources";// 生成xml文件的输出目录
    private final static String AUTHOR = "yc";// 开发人员
    // 数据源配置
    private final static String ORACLE_DRIVER_NAME = "oracle.jdbc.driver.OracleDriver";// ORACLE数据库驱动
    private final static String DATABASE_IP = "192.168.0.129";// 数据库ip
    private final static String ORACLE_DATABASE_NAME = "oracledb";// 数据库名称
    private final static String ORACLE_DB_USERNAME = "familyuat";// 数据库用户
    private final static String ORACLE_DB_PASSWORD = "familyuat";// 数据库口令
    private final static String ORACLE_DB_PORT = "1521";// ORACLE数据库端口
    public static final String ORACLE_DB_SCHEMA = "TEST";
    // 包和数据库表配置
    private final static String MODULE_NAME = "investcommand";// 父包模块名
    private final static String PARENT_PACKAGE = "com.avictc.family";// 父包名。如果为空，将下面子包名必须写全部， 否则就只需写子包名
    private final static String[] TABLE_NAME_LIST = {"T_WORKFLOW_RECORD","T_WORKFLOW_FILE_RELATION"};// 父包模块名
    // 公共配置
    public static final IdType ID_TYPE = IdType.INPUT;
    // 自定义基类
    private final static String SuperEntity = PARENT_PACKAGE + ".common.BaseEntity";
    private final static String SuperController = PARENT_PACKAGE + ".common.BaseController";


    /**
     * 运行main方法即可
     * 1.全局配置
     * 2.数据源配置
     * 3.包配置策略
     * 4.策略配置
     * 5.整合配置
     */
    public static void main(String[] args) {

        //获取项目本地磁盘路径
        String projectPath = System.getProperty("user.dir");
/********************************** 全局配置**********************************/
        // 全局配置
        GlobalConfig gc = new GlobalConfig();

        //生成java文件的存放位置
        gc.setOutputDir(projectPath + "/src/main/java")
                .setAuthor(AUTHOR)//作者署名
                .setFileOverride(true)//是否文件覆盖
                .setIdType(ID_TYPE)//主键策略
                .setBaseResultMap(true)
                .setBaseColumnList(true)//生成sql片段
                .setServiceName("%sService")//设置生成service接口是否首字母是I
                //是否打开输出目录
                .setOpen(false);
/********************************** 数据源配置**********************************/
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.ORACLE)//设置数据库类型
                .setUrl("jdbc:oracle:thin:@" + DATABASE_IP + ":" + ORACLE_DB_PORT + ":" + ORACLE_DATABASE_NAME)
                .setDriverName(ORACLE_DRIVER_NAME)
                .setUsername(ORACLE_DB_USERNAME)
                .setPassword(ORACLE_DB_PASSWORD)
                .setSchemaName(ORACLE_DB_SCHEMA);
/********************************** 包名配置**********************************/
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(PARENT_PACKAGE)
                .setModuleName(MODULE_NAME)
                .setController("controller")
                .setService("service")
                .setServiceImpl("service.impl")
                .setMapper("mapper");

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        List<FileOutConfig> focList = new ArrayList<>();
        focList.add(new FileOutConfig("/templates/mapper.xml.ftl") {
            public File outputFile(TableInfo tableInfo) {
                // 自定义输入文件名称 单模块场景
                return new File(projectPath + OUTPUT_XML_DIR + "/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML);
            }
        });
        cfg.setFileOutConfigList(focList);
/********************************** 策略配置**********************************/

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setCapitalMode(true)//全局大小写命名
                /* .setFieldPrefix("c_","d_","n_","C_","D_","N_","c","d","n")*/
                .setTablePrefix("t_", "c_","d_","n_")
                .setColumnNaming(NamingStrategy.underline_to_camel)//数据库字段是否下划线转驼峰
                .setNaming(NamingStrategy.underline_to_camel)//数据库表映射到实体的命名策略
//                .setSuperEntityClass(SuperEntity)
                .setEntityLombokModel(true)
//                .setSuperControllerClass(SuperController)
                .setInclude(TABLE_NAME_LIST)
                .setSuperEntityColumns("id")
                .setControllerMappingHyphenStyle(true);

        // 配置整合 代码生成器
        AutoGenerator mpg = new AutoGenerator();
        //配置信息添加至 全局配置容器
        mpg.setGlobalConfig(gc)
                .setStrategy(strategy)
                .setDataSource(dsc)
                .setPackageInfo(pc)
                .setCfg(cfg)
                .setTemplate(new TemplateConfig().setXml(null))
                .setTemplateEngine(new FreemarkerTemplateEngine())// 选择 freemarker 引擎需要指定如下加，注意 pom 依赖必须有！
                .execute();//执行
    }
    /*************************************MYSQL代码生成  End *************************************/

}

